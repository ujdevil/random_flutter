import 'package:flutter/material.dart';
import 'dart:math';

void main() => runApp(MyApp());

const MaxValue = 20;
const NumRolls = 100; //500000;
int _highestValue = NumRolls; // global

class Counter {
  int value = 0;

  Counter();

  Widget build(int index) {
    return Container(
        // child: Text("${index + 1} : $value"), padding: EdgeInsets.all(4.0));
        padding: EdgeInsets.all(4.0),
        child: Row(children: <Widget>[
          Text("${index + 1}"),
          Expanded(
              child: LinearProgressIndicator(
                  value: value.toDouble() / _highestValue))
        ]));
  }

  @override
  String toString() {
    return '${this.value}';
  }
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Random',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter Random (D20)'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  Random _random;
  final List<Counter> _counters = new List(); //MaxValue);

  @override
  void initState() {
    for (var i = 0; i < MaxValue; ++i) {
      _counters.add(new Counter());
    }

    _random = new Random();

    super.initState();
  }

  void _incrementCounter() {
    _counters.forEach((item) => item.value = 0);

    for (var i = 0; i < NumRolls; ++i) {
      var r = _random.nextInt(MaxValue);
      _counters[r].value++;
    }

    print("avant: $_counters");

    _highestValue = 1;
    _counters.forEach((item) =>
        item.value > _highestValue ? _highestValue = item.value : null);

    print("apres: $_counters");

    setState(() {});

    print("${_counters.length}");
    for (final c in _counters) {
      c.value++;
      // c.refresh();
    }
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: ListView.builder(
            itemCount: _counters.length,
            itemBuilder: (context, index) {
              return _counters[index].build(index);
            }),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Randomize',
        child: Icon(Icons.autorenew),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
